﻿// https://learn.microsoft.com/en-us/dotnet/standard/commandline/

// TODO should check size (i.e., 4:3, 16:9) since it breaks formatting.

// +---desktop
// |       1 SONG 420
// |       2 lesson.pptx
// |       3 SHARED calendar
// |
// \---documents
//     +---shared
//     |       calendar.pptx
//     |
//     \---songs
//             420.pptx

// cd c:\user\me
// spc.exe --shared-folder=c:\user\me\documents\shared
//         --song-folder=c:\user\me\documents\songs
//         --file="desktop\2 lesson.pptx"
//         --file "desktop\1 SONG 420"
//         --file:"desktop\3 SHARED calendar"
//         --output-file "..\merged.pptx"
//
// OR
//
// cd c:\user\me\desktop
// spc.exe --shared-folder=c:\user\me\documents\shared
//         --song-folder=c:\user\me\documents\songs
//         --output-file "..\merged.pptx"

using System.Text.RegularExpressions;
using DocumentFormat.OpenXml.ExtendedProperties;
using DocumentFormat.OpenXml.Packaging;
using DocumentFormat.OpenXml.Presentation;

Option<string> songFolderOption = new("--song-folder", "Folder containing song files.");
Option<string> sharedFolderOption = new("--shared-folder", "Folder containing shared files.");
Option<string> outputFileOption = new("--output-file", getDefaultValue: () => "merged.pptx", "Name of the output file..");

RootCommand rootCommand = new("TODO: what does this app do?") { songFolderOption, outputFileOption, sharedFolderOption };

rootCommand.SetHandler((songFolder, sharedFolder, outputFile) => CreateMergedPresentation(songFolder, sharedFolder, outputFile), songFolderOption, sharedFolderOption, outputFileOption);

await rootCommand.InvokeAsync(args);

static void CreateMergedPresentation(string songFolder, string sharedFolder, string outputFile)
{
    // TODO error if any files begin with the same number
    // TODO error if SONG or SHARED files don't exist

    var x = Directory.GetFiles(".")
                     .Where(f => Regex.IsMatch(Path.GetFileName(f), @"^\d+\s"))
                     .OrderBy(f => int.Parse(Regex.Match(Path.GetFileName(f), @"^(\d+)").Value))
                     .Select(f =>
                     {
                         if (Regex.IsMatch(Path.GetFileName(f), @"^\d+\s+SONG\s+"))
                         { // if "SONG"
                             return $"{songFolder}/{Regex.Replace(Path.GetFileName(f), @"^\d+\s+SONG\s+", "")}.pptx";
                         }
                         else if (Regex.IsMatch(Path.GetFileName(f), @"^\d+\s+SHARED\s+"))
                         { // SHARED
                             return $"{sharedFolder}/{Regex.Replace(Path.GetFileName(f), @"^\d+\s+SHARED\s+", "")}.pptx";
                         }
                         else
                         { // ASSUME current directory file?
                             return $"{f}";
                         }
                     });

    List<SlideSource> sources = new();

    foreach (var f in x)
    {
        Console.WriteLine($"Appending {f} to {outputFile}");
        sources.Add(new SlideSource(new PmlDocument(f), true));

        using PresentationDocument pd = PresentationDocument.Open(f, false);

        // TODO add check for all files to be the same format (i.e., wide or 4:3)
        if (pd.ExtendedFilePropertiesPart != null && pd.ExtendedFilePropertiesPart.Properties.PresentationFormat != null)
        {
            Console.WriteLine(pd.ExtendedFilePropertiesPart.Properties.PresentationFormat.Text);
        }
        else
        {
            Console.WriteLine("TODO this was null.  I'm not sure why.  You may have issues.  Ensure all slide XXX is widescreen.");
        }
    }

    PresentationBuilder.BuildPresentation(sources, outputFile);
}

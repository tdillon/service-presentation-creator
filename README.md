# Service Presentation Creator

TODO: info/instructions about project

## Developer Info

- http://www.ericwhite.com/blog/presentationbuilder-developer-center/

### Developer Setup

- download .net sdk
- Add .NET SDK folder to path.  e.g., `/c/NETCore/dotnet-sdk-6.0.414-win-x64`
- Install [C# Dev Kit](https://marketplace.visualstudio.com/items?itemName=ms-dotnettools.csdevkit) VS Code extension

```shell
# Run production build locally.
dotnet publish --configuration Release
./bin/Release/net6.0/win-x64/publish/service-presentation-creator.exe --help
```

```shell
# Run project directly from a directory outside of the repo.
dotnet run --project ~/path/to/repo/service-presentation-creator.csproj --version
```

### TODOs

- Add more flexible "folder" options.  e.g., be able to 'dynamically' add various folder options that link to prefixed files.
  - Currently `spc --song-folder=/songs/` is used to get files named `# SONG <song number>`.  But the `--song-folder` option and "SONG" prefix are hard coded.
  - It would be nice if a flexible setup could be used.  I don't know what the proper way to do this is.  e.g., `--folder=FANCYSONGS:/foo/bar/songs2` would look for `# FANCYSONGS <song number>` file in `/foo/bar/songs2`.
- Add testing to pipeline
  - all options
  - ordering (e.g., "1 file.pptx", "2 file.pptx", "11 file.pptx")
- Better error handling.
- Better output.
- Better help/documenation.
- Use GitLab releases.
- Cleanup code
- Add option to add files.  e.g., `--file="1 SONG 123" --file "../../4 lesson.pptx"`.  Would this be useful?
- Add argument for input directory.  Maybe not needed.

### Release Steps

Update the version number in `Directory.Build.props` when you want to deploy.
The master branch build will deploy the application.
